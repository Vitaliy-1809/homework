import React from 'react';
import './Home.scss';
import Loader from '../../components/Loader/Loader';
import ProductList from '../../components/ProductList/ProductList';
import { useSelector } from 'react-redux';
import { productsSelectors } from '../../store/products';

const Home = () => {
    const isLoading = useSelector(productsSelectors.isProductsLoading());

    return (
        <div className='products container'>
            <h1 className='header-title'>Musical instruments</h1>
            {isLoading && <Loader />}
            {!isLoading && <ProductList />}
        </div>
    );
}

export default Home;