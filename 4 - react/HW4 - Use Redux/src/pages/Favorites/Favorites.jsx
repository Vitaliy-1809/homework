import React, { useEffect } from 'react';
import ProductList from '../../components/ProductList/ProductList';
import { useDispatch, useSelector } from 'react-redux';
import { favoritesOperations, favoritesSelectors } from '../../store/favorites';

const Favorites = () => {
    const favorites = useSelector(favoritesSelectors.getFavorites());
    const dispatch = useDispatch();

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites'));

        if (favorites) {
            dispatch(favoritesOperations.addFavorites(favorites));
        }

    }, [dispatch]);

    return (
        <div className='products container'>
            <h1 className='header-title'>Favorites</h1>
            {favorites.length === 0 &&
                <p>Nothing has been added to favorites...</p>
            }
            <ProductList />
        </div>
    );
}

export default Favorites;