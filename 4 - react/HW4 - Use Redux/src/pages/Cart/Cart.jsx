import React, { useEffect } from 'react';
import ProductList from '../../components/ProductList/ProductList';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { cartOperations, cartSelectors } from '../../store/cart';

const Cart = () => {
    const cart = useSelector(cartSelectors.getCart());
    const dispatch = useDispatch();

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem('cart'));

        if (cart) {
            dispatch(cartOperations.addToCart(cart));
        }
    }, [dispatch]);

    return (
        <div className='products container'>
            <h1 className='header-title'>Cart</h1>
            {cart.length === 0 &&
                <p>Nothing has been added to cart...</p>
            }
            <ProductList />
        </div>
    );
}

export default Cart;