import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = ({ className, backgroundColor, text, onClick }) => {
    return (
        <button
            className={className}
            style={{ backgroundColor: backgroundColor }}
            onClick={onClick}
        >
            {text}
        </button>
    );
}

Button.defaultProps = {
    className: 'btn',
    backgroundColor: 'none',
    text: 'button',
    onClick: () => { }
};

Button.propTypes = {
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.node,
    onClick: PropTypes.func
};

export default Button;