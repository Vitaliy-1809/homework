import React from 'react';
import Modal from './Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { modalOperations } from '../../store/modal';
import { productsOperations, productsSelectors } from '../../store/products';

const Modal1 = ({ productCard }) => {
    const products = useSelector(productsSelectors.getProducts());
    const dispatch = useDispatch();

    const addToCart = productCart => {
        let cartProducts = [];

        if (products.find(product => product.id === productCart.id)) {
            cartProducts = products.map(product =>
                product.id === productCart.id ? { ...product, inCart: true } : product
            )

            dispatch(productsOperations.addProducts(cartProducts));
        }

        const productsInCart = cartProducts.filter(product => product.inCart);
        localStorage.setItem('cart', JSON.stringify(productsInCart));

        closeModal();
    }

    const closeModal = () => {
        dispatch(modalOperations.toggleModal(false));
    }

    const modalButtons = (
        <div>
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Ok'
                onClick={() => addToCart(productCard)}
            />
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Cancel'
                onClick={closeModal}
            />
        </div>
    );

    return (
        <Modal
            modalClassName='modal__window'
            headerClassName='modal__header'
            header='Adding an item to the cart'
            closeButton
            text='Do you confirm?'
            actions={modalButtons}
            closeModal={closeModal}
        />
    );
}

Modal1.propTypes = {
    productCard: PropTypes.object.isRequired
}

export default Modal1;