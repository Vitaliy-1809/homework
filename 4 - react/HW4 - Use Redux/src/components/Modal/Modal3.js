import React from 'react';
import Modal from './Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { modalOperations } from '../../store/modal';
import { productsOperations, productsSelectors } from '../../store/products';

const Modal3 = ({ productCard }) => {
    const products = useSelector(productsSelectors.getProducts());
    const dispatch = useDispatch();
    const history = useHistory();

    const deleteFromCart = removingProduct => {
        const updatedProducts = products.map(product => {
            if (product.id === removingProduct.id) return { ...product, inCart: false }
            return product
        })

        const cartStorage = JSON.parse(localStorage.getItem('cart'));
        const updateCartStorage = cartStorage.filter(product => product.id !== removingProduct.id);

        if (cartStorage.length === 1) {
            localStorage.removeItem('cart');
        } else {
            localStorage.setItem('cart', JSON.stringify(updateCartStorage));
        }

        dispatch(productsOperations.removeProducts(updatedProducts));

        closeModal();
    }

    const closeModal = () => {
        dispatch(modalOperations.toggleModal(false));
    }

    const modalButtons = (
        <div>
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Ok'
                onClick={() => {
                    deleteFromCart(productCard);
                    history.go(0);
                }}
            />
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Cancel'
                onClick={closeModal}
            />
        </div>
    );

    return (
        <Modal
            modalClassName='modal__window'
            headerClassName='modal__header'
            header='Removing a product from the cart'
            closeButton
            text='Do you confirm?'
            actions={modalButtons}
            closeModal={closeModal}
        />
    );
}

Modal3.propTypes = {
    productCard: PropTypes.object.isRequired
}

export default Modal3;