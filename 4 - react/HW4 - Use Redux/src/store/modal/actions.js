import types from './types';

const toggleModal = (content) => ({
    type: types.TOGGLE_MODAL,
    payload: content
})

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    toggleModal
}