import actions from './actions';
const { addToCart } = actions;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    addToCart
}