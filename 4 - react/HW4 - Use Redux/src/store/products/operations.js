import actions from './actions';
import axios from 'axios';

const { addProducts, removeProducts } = actions;

const fetchProducts = () => (dispatch) => {
    axios('/products.json')
        .then(res => {
            const dataProducts = res.data.map(product => ({ ...product, isFavorite: false, inCart: false }));
            const favoritesStorage = JSON.parse(localStorage.getItem('favorites'));
            const cartStorage = JSON.parse(localStorage.getItem('cart'));

            let updatedData = dataProducts;

            if (cartStorage) {
                updatedData = dataProducts.map(product => {
                    const cartProduct = cartStorage.find(cartProduct => product.id === cartProduct.id);
                    return { ...product, inCart: cartProduct ? true : product.inCart }
                })
            }

            if (favoritesStorage) {
                updatedData = updatedData.map(product => {
                    const favoriteProduct = favoritesStorage.find(favProduct => product.id === favProduct.id);
                    return { ...product, isFavorite: favoriteProduct ? true : product.isFavorite }
                })
            }

            dispatch(actions.addProducts(updatedData));
        })
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    fetchProducts,
    addProducts, 
    removeProducts
}