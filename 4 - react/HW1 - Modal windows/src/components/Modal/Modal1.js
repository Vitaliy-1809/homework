import React, { Component } from 'react';
import Modal from './Modal';
import Button from '../Button/Button';

class Modal1 extends Component {
    
    render() {
        const { closeModal } = this.props;

        const modalButtons = (
            <div>
                <Button
                    className='btn btn-submit'
                    backgroundColor='#b3382c'
                    text='Ok'
                    onClick={closeModal}
                />
                <Button
                    className='btn btn-submit'
                    backgroundColor='#b3382c'
                    text='Cancel'
                    onClick={closeModal}
                />
            </div>
        );

        return (
            <Modal
                modalClassName='modal__window modal__window--red'
                headerClassName='modal__header modal__header--red'
                header='Do you want to delete this file?'
                closeButton
                text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                actions={modalButtons}
                closeModal={closeModal}
            />
        );
    }
}

export default Modal1;