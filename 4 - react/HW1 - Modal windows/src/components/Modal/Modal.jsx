import React, { Component } from 'react';
import './Modal.scss';
import Button from '../Button/Button';

class Modal extends Component {

    render() {
        const { modalClassName, headerClassName, header, closeButton, text, actions, closeModal } = this.props;

        const crossIcon =
            <svg width="20px" height="20px" viewBox="0 0 414.298 414.299" fill="#fff">
                <path d="M3.663,410.637c2.441,2.44,5.64,3.661,8.839,3.661c3.199,0,6.398-1.221,8.839-3.661l185.809-185.81l185.81,185.811c2.44,2.44,5.641,3.661,8.84,3.661c3.198,0,6.397-1.221,8.839-3.661c4.881-4.881,4.881-12.796,0-17.679l-185.811-185.81l185.811-185.81c4.881-4.882,4.881-12.796,0-17.678c-4.882-4.882-12.796-4.882-17.679,0l-185.81,185.81L21.34,3.663c-4.882-4.882-12.796-4.882-17.678,0c-4.882,4.881-4.882,12.796,0,17.678l185.81,185.809L3.663,392.959C-1.219,397.841-1.219,405.756,3.663,410.637z" />
            </svg>

        const closeButtonProps =
            <Button
                className='btn btn-close'
                text={crossIcon}
                onClick={closeModal}
            />

        return (
            <>
                <div className='modal' onClick={closeModal}>
                    <div className={modalClassName} onClick={(e) => e.stopPropagation()}>
                        <div className={headerClassName}>
                            <h2 className='header__title'>
                                {header}
                            </h2>
                            {
                                closeButton && closeButtonProps
                            }
                        </div>
                        <div className='modal__body'>
                            {text}
                            <div className='modal__buttons'>
                                {actions}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Modal;