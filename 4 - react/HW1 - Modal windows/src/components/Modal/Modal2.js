import React, { Component } from 'react';
import Modal from './Modal';
import Button from '../Button/Button';

class Modal2 extends Component {
    
    render() {
        const { closeModal } = this.props;

        const modalButtons = (
            <div>
                <Button
                    className='btn btn-submit'
                    backgroundColor='#104d27'
                    text='Yes'
                    onClick={closeModal}
                />
                <Button
                    className='btn btn-submit'
                    backgroundColor='#104d27'
                    text='No'
                    onClick={closeModal}
                />
            </div>
        );

        return (
            <Modal
                modalClassName='modal__window modal__window--green'
                headerClassName='modal__header modal__header--green'
                header='Would you like to continue?'
                closeButton={false}
                text='By clicking Yes, I certify that I have read and agree to the Privacy Policy and Terms of Service'
                actions={modalButtons}
                closeModal={closeModal}
            />
        );
    }
}

export default Modal2;