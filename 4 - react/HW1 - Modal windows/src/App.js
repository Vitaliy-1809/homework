import React, { Component } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal1 from './components/Modal/Modal1';
import Modal2 from './components/Modal/Modal2';

class App extends Component {
    
    state = {
        modal: false
    }

    openModal = (content) => {
        this.setState({
            modal: content
        });
    }

    closeModal = () => {
        this.setState({
            modal: false
        });
    }

    render() {
        return (
            <>
                <div className='button-wrapper'>
                    <Button
                        className='btn btn-open-modal'
                        backgroundColor='#d44637'
                        text='Open first modal'
                        onClick={() => this.openModal(
                            <Modal1
                                closeModal={this.closeModal}
                            />)
                        }
                    />
                    <Button
                        className='btn btn-open-modal'
                        backgroundColor='#176937'
                        text='Open second modal'
                        onClick={() => this.openModal(
                            <Modal2
                                closeModal={this.closeModal}
                            />)
                        }
                    />
                </div>
                {this.state.modal}
            </>
        );
    }
}

export default App;