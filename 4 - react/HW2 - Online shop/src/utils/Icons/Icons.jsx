import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as allIcons from './index';

class Icons extends Component {
    render() {
        const { type, color, className, svgClassName, ...restProps } = this.props;

        const currentIcon = allIcons[type];

        if (!currentIcon) {
            return null
        }

        return (
            <div className={className} {...restProps}>
                {currentIcon(color, svgClassName)}
            </div>
        );
    }
}

Icons.defaultProps = {
    color: 'black',
    className: 'svg-wrapper',
    svgClassName: 'svg'
};

Icons.propTypes = {
    type: PropTypes.string.isRequired,
    color: PropTypes.string,
    className: PropTypes.string,
    svgClassName: PropTypes.string
};

export default Icons;