import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';
import Loader from './components/Loader/Loader';
import ProductList from './components/ProductList/ProductList';

class App extends Component {

    state = {
        products: [],
        isLoading: true,
        modal: false,
        activeProduct: ''
    }

    componentDidMount() {
        setTimeout(() => {
            axios('/products.json')
                .then(res => {
                    const dataProducts = res.data.map(product => ({ ...product, isFavorite: false }));
                    const favoritesStorage = JSON.parse(localStorage.getItem('favorites'));

                    if (favoritesStorage) {
                        const updatedData = dataProducts.map(product => {
                            const favoriteProduct = favoritesStorage.find(favProduct => product.id === favProduct.id);
                            return { ...product, isFavorite: favoriteProduct ? true : product.isFavorite }
                        })

                        this.setState({ products: updatedData, isLoading: false })
                    } else {
                        this.setState({ products: dataProducts, isLoading: false })
                    }
                })
        }, 1000)
    }

    addToCart = () => {
        const { activeProduct } = this.state;

        const cart = JSON.parse(localStorage.getItem('cart'));

        if (cart) {
            cart.push(activeProduct);
            localStorage.setItem('cart', JSON.stringify(cart));
        } else {
            localStorage.setItem('cart', JSON.stringify([activeProduct]))
        }

        this.closeModal();
    }

    addToFavorite = id => {
        const { products } = this.state;

        let favoriteProducts = [];

        if (products.find(product => product.id === id)) {
            favoriteProducts = products.map(product =>
                product.id === id ? { ...product, isFavorite: !product.isFavorite } : product
            )

            this.setState({ products: favoriteProducts })
        }

        const favoriteProductsID = favoriteProducts.filter(product => product.isFavorite ? product.id : '');
        localStorage.setItem('favorites', JSON.stringify(favoriteProductsID))
    }

    openModal = (content, product) => {
        this.setState({ modal: content, activeProduct: product });
    }

    closeModal = () => {
        this.setState({ modal: false });
    }

    render() {
        const { modal, products, isLoading } = this.state;

        return (
            <>
                <div className='products container'>
                    <h1 className='header-title'>Musical instruments</h1>
                    {isLoading && <Loader />}
                    {!isLoading && <ProductList
                        products={products}
                        addToCart={this.addToCart}
                        addToFavorite={this.addToFavorite}
                        openModal={this.openModal}
                        closeModal={this.closeModal}
                    />}
                </div>
                {modal}
            </>
        );
    }
}

export default App;