import React, { Component } from 'react';
import './ProductList.scss';
import Product from '../Product/Product';
import PropTypes from 'prop-types';

class ProductList extends Component {
    render() {
        const { products, addToCart, addToFavorite, openModal, closeModal } = this.props;

        const productCards = products.map(product => (
            <Product
                key={product.id}
                productCard={product}
                isFavorite={product.isFavorite}
                addToCart={addToCart}
                addToFavorite={addToFavorite}
                openModal={openModal}
                closeModal={closeModal}
            />
        ));

        return (
            <div className='products-list'>
                {productCards}
            </div>
        );
    }
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        img: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    })).isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default ProductList;