import React, { Component } from 'react';
import './Product.scss';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import Modal1 from '../Modal/Modal1';
import Icons from '../../utils/Icons/Icons';

class Product extends Component {
    render() {
        const { productCard, isFavorite, addToCart, addToFavorite, openModal, closeModal } = this.props;

        const starIcon =
            <Icons
                type='starIcon'
                color={isFavorite ? '#f90' : '#fc9'}
                className='svg-wrapper star-icon'
                svgClassName='svg-star'
                title={isFavorite ? 'remove from favorites' : 'add to favorites'}
            />

        return (
            <div className='product'>
                <div className='product__header'>
                    <p className='product__code'>code: {productCard.code}</p>
                    <Button
                        className='btn btn-star'
                        text={starIcon}
                        onClick={() => addToFavorite(productCard.id)}
                    />
                </div>
                <a href={productCard.img} className='product-link'>
                    <div className='image-wrapper'>
                        <img
                            className='product__image'
                            alt='product'
                            src={productCard.img}
                            title='enlarge image'
                        />
                    </div>
                    <p className='product__name'>{productCard.name}</p>
                </a>
                <p className='product__color'>color: {productCard.color}</p>
                <div className='items-wrapper'>
                    <p className='product__price'>{productCard.price}</p>
                    <Button
                        className='btn btn-open-modal'
                        backgroundColor='#1e1e20'
                        text='Add to cart'
                        onClick={() => openModal(
                            <Modal1 closeModal={closeModal} addToCart={addToCart} />,
                            productCard)
                        }
                    />
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    productCard: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        img: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    }).isRequired,
    isFavorite: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
}

export default Product;