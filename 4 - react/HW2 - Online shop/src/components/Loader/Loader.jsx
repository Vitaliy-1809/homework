import React, { Component } from 'react';
import './Loader.scss';

class Loader extends Component {
    render() {
        return (
            <div>
                <div className="loader">Loading...</div>
            </div>
        );
    }
}

export default Loader;