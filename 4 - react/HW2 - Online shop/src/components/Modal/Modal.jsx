import React, { Component } from 'react';
import './Modal.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import Icons from '../../utils/Icons/Icons';

class Modal extends Component {

    render() {
        const { modalClassName, headerClassName, header, closeButton, text, actions, closeModal } = this.props;

        const crossIcon =
            <Icons
                type='crossIcon'
                color='#fff'
                className='svg-wrapper cross-icon'
                svgClassName='svg'
            />

        const closeButtonProps =
            <Button
                className='btn btn-close'
                text={crossIcon}
                onClick={closeModal}
            />

        return (
            <>
                <div className='modal' onClick={closeModal}>
                    <div className={modalClassName} onClick={(e) => e.stopPropagation()}>
                        <div className={headerClassName}>
                            <h2 className='header__title'>
                                {header}
                            </h2>
                            {
                                closeButton && closeButtonProps
                            }
                        </div>
                        <div className='modal__body'>
                            {text}
                            <div className='modal__buttons'>
                                {actions}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

Modal.defaultProps = {
    modalClassName: 'modal',
    headerClassName: 'modal__header',
    header: 'Modal window header',
    closeButton: true,
    text: 'Modal window main text'
};

Modal.propTypes = {
    modalClassName: PropTypes.string,
    headerClassName: PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.node.isRequired,
    closeModal: PropTypes.func.isRequired
};

export default Modal;