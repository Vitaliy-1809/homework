import React, { Component } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {

    render() {
        const { className, backgroundColor, text, onClick } = this.props;

        return (
            <button
                className={className}
                style={{ backgroundColor: backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

Button.defaultProps = {
    className: 'btn',
    backgroundColor: 'none',
    text: 'button',
    onClick: () => { }
};

Button.propTypes = {
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.node,
    onClick: PropTypes.func
};

export default Button;