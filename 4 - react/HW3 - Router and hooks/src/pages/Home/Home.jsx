import React from 'react';
import './Home.scss';
import PropTypes from 'prop-types';
import Loader from '../../components/Loader/Loader';
import ProductList from '../../components/ProductList/ProductList';

const Home = ({ products, isLoading, addToCart, addToFavorite, openModal, closeModal }) => {
    return (
        <div className='products container'>
            <h1 className='header-title'>Musical instruments</h1>
            {isLoading && <Loader />}
            {!isLoading && <ProductList
                products={products}
                addToCart={addToCart}
                addToFavorite={addToFavorite}
                openModal={openModal}
                closeModal={closeModal}
            />}
        </div>
    );
}

Home.propTypes = {
    products: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default Home;