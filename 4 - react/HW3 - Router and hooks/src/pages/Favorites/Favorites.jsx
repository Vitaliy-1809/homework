import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ProductList from '../../components/ProductList/ProductList';

const Favorites = ({ addToCart, addToFavorite, openModal, closeModal }) => {
    const [favorites, setFavorites] = useState([]);

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites'));

        if (favorites) {
            setFavorites(favorites);
        }

    }, []);

    return (
        <div className='products container'>
            <h1 className='header-title'>Favorites</h1>
            {favorites.length === 0 &&
                <p>Nothing has been added to favorites...</p>
            }
            <ProductList
                products={favorites}
                addToCart={addToCart}
                addToFavorite={addToFavorite}
                openModal={openModal}
                closeModal={closeModal}
            />
        </div>
    );
}

Favorites.propTypes = {
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default Favorites;