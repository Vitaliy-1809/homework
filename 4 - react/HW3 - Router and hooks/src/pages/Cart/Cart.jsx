import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ProductList from '../../components/ProductList/ProductList';

const Cart = ({ deleteFromCart, openModal, closeModal }) => {
    const [cart, setCart] = useState([]);

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem('cart'));

        if (cart) {
            setCart(cart);
        }
    }, []);

    return (
        <div className='products container'>
            <h1 className='header-title'>Cart</h1>
            {cart.length === 0 &&
                <p>Nothing has been added to cart...</p>
            }
            <ProductList
                products={cart}
                deleteFromCart={deleteFromCart}
                openModal={openModal}
                closeModal={closeModal}
            />
        </div>
    );
}

Cart.propTypes = {
    deleteFromCart: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default Cart;