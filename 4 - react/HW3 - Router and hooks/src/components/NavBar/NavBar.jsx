import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.scss';

const NavBar = () => {
    return (
        <ul className='navbar'>
            <li>
                <NavLink exact to='/' className='navbar__link' activeClassName='navbar__link--active'>Home</NavLink>
            </li>
            <li>
                <NavLink exact to='/favorites' className='navbar__link' activeClassName='navbar__link--active'>Favorites</NavLink>
            </li>
            <li>
                <NavLink exact to='/cart' className='navbar__link' activeClassName='navbar__link--active'>Cart</NavLink>
            </li>
        </ul>
    );
}

export default NavBar;