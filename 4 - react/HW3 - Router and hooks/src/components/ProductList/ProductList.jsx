import React from 'react';
import './ProductList.scss';
import Product from '../Product/Product';
import PropTypes from 'prop-types';

const ProductList = ({ products, addToCart, addToFavorite, deleteFromCart, openModal, closeModal }) => {
    const productCards = products.map(product => (
        <Product
            key={product.id}
            productCard={product}
            isFavorite={product.isFavorite}
            inCart={product.inCart}
            addToCart={addToCart}
            addToFavorite={addToFavorite}
            deleteFromCart={deleteFromCart}
            openModal={openModal}
            closeModal={closeModal}
        />
    ));

    return (
        <div className='products-list'>
            {productCards}
        </div>
    );
}

ProductList.defaultProps = {
    addToCart: () => {},
    addToFavorite: () => {},
    deleteFromCart: () => {}
};

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        img: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    })).isRequired,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
    deleteFromCart: PropTypes.func,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default ProductList;