import React from 'react';
import './Product.scss';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import Icons from '../../utils/Icons/Icons';
import { useHistory, useLocation } from 'react-router';
import Modal1 from '../Modal/Modal1';
import Modal2 from '../Modal/Modal2';
import Modal3 from '../Modal/Modal3';

const Product = ({ productCard, isFavorite, inCart, addToCart, addToFavorite, deleteFromCart, openModal, closeModal }) => {
    const location = useLocation();
    const history = useHistory();

    const starIcon =
        <Icons
            type='starIcon'
            color={isFavorite ? '#f90' : '#fc9'}
            className='svg-wrapper star-icon'
            svgClassName='svg-star'
            title={isFavorite ? 'remove from favorites' : 'add to favorites'}
        />

    const crossIcon =
        <Icons
            type='crossIcon'
            color='#000'
            className='svg-wrapper cross-icon'
            svgClassName='svg'
            title={inCart && 'remove from cart'}
        />

    return (
        <div className='product'>
            <div className='product__header'>
                <p className='product__code'>code: {productCard.code}</p>
                {location.pathname === '/' && <Button
                    className='btn btn-star'
                    text={starIcon}
                    onClick={() => addToFavorite(productCard.id)}
                />}
                {location.pathname === '/favorites' && <Button
                    className='btn btn-star'
                    text={starIcon}
                    onClick={() => {
                        addToFavorite(productCard.id);
                        history.go(0);
                    }}
                />}
                {location.pathname === '/cart' && <Button
                    className='btn btn-close'
                    text={crossIcon}
                    onClick={() => {
                        openModal(<Modal3
                            closeModal={closeModal}
                            deleteFromCart={deleteFromCart}
                            productCard={productCard}
                        />)
                    }}
                />}
            </div>
            <a href={productCard.img} className='product-link'>
                <div className='image-wrapper'>
                    <img
                        className='product__image'
                        alt='product'
                        src={productCard.img}
                        title='enlarge image'
                    />
                </div>
                <p className='product__name'>{productCard.name}</p>
            </a>
            <p className='product__color'>color: {productCard.color}</p>
            <div className='items-wrapper'>
                <p className='product__price'>{productCard.price}</p>
                {location.pathname !== '/cart' && <Button
                    className='btn btn-open-modal'
                    backgroundColor='#1e1e20'
                    text='Add to cart'
                    onClick={() => {
                        openModal(<Modal1
                            closeModal={closeModal}
                            addToCart={addToCart}
                            productCard={productCard}
                        />)
                    }}
                />}
                {location.pathname === '/cart' && <Button
                    className='btn btn-open-modal btn-buy'
                    backgroundColor='#1e1e20'
                    text='Buy'
                    onClick={() => {
                        openModal(<Modal2
                            closeModal={closeModal}
                            deleteFromCart={deleteFromCart}
                            productCard={productCard}
                        />)
                    }}
                />}
            </div>
        </div>
    );
}

Product.defaultProps = {
    addToFavorite: () => {},
    deleteFromCart: () => {},
};

Product.propTypes = {
    productCard: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        img: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    }).isRequired,
    isFavorite: PropTypes.bool.isRequired,
    inCart: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func,
    deleteFromCart: PropTypes.func,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
}

export default Product;