import React from 'react';
import './Loader.scss';

const Loader = () => {
    return (
        <div>
            <div className="loader">Loading...</div>
        </div>
    );
}

export default Loader;