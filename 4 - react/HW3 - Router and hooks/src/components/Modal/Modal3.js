import React from 'react';
import Modal from './Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';

const Modal3 = ({ closeModal, deleteFromCart, productCard }) => {
    const history = useHistory();

    const modalButtons = (
        <div>
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Ok'
                onClick={() => {
                    deleteFromCart(productCard);
                    history.go(0);
                }}
            />
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Cancel'
                onClick={closeModal}
            />
        </div>
    );

    return (
        <Modal
            modalClassName='modal__window'
            headerClassName='modal__header'
            header='Removing a product from the cart'
            closeButton
            text='Do you confirm?'
            actions={modalButtons}
            closeModal={closeModal}
        />
    );
}

Modal3.propTypes = {
    closeModal: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
    productCard: PropTypes.object.isRequired
}

export default Modal3;