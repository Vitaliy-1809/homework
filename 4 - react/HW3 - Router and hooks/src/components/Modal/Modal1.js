import React from 'react';
import Modal from './Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

const Modal1 = ({ closeModal, addToCart, productCard }) => {
    const modalButtons = (
        <div>
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Ok'
                onClick={() => addToCart(productCard)}
            />
            <Button
                className='btn btn-submit'
                backgroundColor='#28282b'
                text='Cancel'
                onClick={closeModal}
            />
        </div>
    );

    return (
        <Modal
            modalClassName='modal__window'
            headerClassName='modal__header'
            header='Adding an item to the cart'
            closeButton
            text='Do you confirm?'
            actions={modalButtons}
            closeModal={closeModal}
        />
    );
}

Modal1.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
    productCard: PropTypes.object.isRequired
}

export default Modal1;