import React, { useEffect, useState } from 'react';
import './App.scss';
import axios from 'axios';
import AppRoutes from './routes/AppRoutes/AppRoutes';
import NavBar from './components/NavBar/NavBar';

const App = () => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [modal, setModal] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            axios('/products.json')
                .then(res => {
                    const dataProducts = res.data.map(product => ({ ...product, isFavorite: false, inCart: false }));
                    const favoritesStorage = JSON.parse(localStorage.getItem('favorites'));
                    const cartStorage = JSON.parse(localStorage.getItem('cart'));

                    let updatedData = dataProducts;

                    if (cartStorage) {
                        updatedData = dataProducts.map(product => {
                            const cartProduct = cartStorage.find(cartProduct => product.id === cartProduct.id);
                            return { ...product, inCart: cartProduct ? true : product.inCart }
                        })
                    }

                    if (favoritesStorage) {
                        updatedData = updatedData.map(product => {
                            const favoriteProduct = favoritesStorage.find(favProduct => product.id === favProduct.id);
                            return { ...product, isFavorite: favoriteProduct ? true : product.isFavorite }
                        })
                    }

                    setProducts(updatedData);
                    setIsLoading(false);
                })
        }, 1000)
    }, []);

    const addToCart = productCart => {
        let cartProducts = [];

        if (products.find(product => product.id === productCart.id)) {
            cartProducts = products.map(product =>
                product.id === productCart.id ? { ...product, inCart: true } : product
            )

            setProducts(cartProducts);
        }

        const productsInCart = cartProducts.filter(product => product.inCart);
        localStorage.setItem('cart', JSON.stringify(productsInCart));

        closeModal();
    }

    const addToFavorite = id => {
        let favoriteProducts = [];

        if (products.find(product => product.id === id)) {
            favoriteProducts = products.map(product =>
                product.id === id ? { ...product, isFavorite: !product.isFavorite } : product
            )
            setProducts(favoriteProducts);
        }

        const favoriteProductsID = favoriteProducts.filter(product => product.isFavorite ? product.id : '');
        localStorage.setItem('favorites', JSON.stringify(favoriteProductsID))
    }

    const deleteFromCart = removingProduct => {
        const updatedProducts = products.map(product => {
            if (product.id === removingProduct.id) return {...product, inCart: false}
            return product
        })

        const cartStorage = JSON.parse(localStorage.getItem('cart'));
        const updateCartStorage = cartStorage.filter(product => product.id !== removingProduct.id);

        if (cartStorage.length === 1) {
            localStorage.removeItem('cart');
        } else {
            localStorage.setItem('cart', JSON.stringify(updateCartStorage));
        }

        setProducts(updatedProducts);

        closeModal();
    }

    const openModal = (content) => {
        setModal(content);
    }

    const closeModal = () => {
        setModal(false);
    }

    return (
        <>
            <NavBar />
            <AppRoutes
                products={products}
                isLoading={isLoading}
                addToCart={addToCart}
                addToFavorite={addToFavorite}
                deleteFromCart={deleteFromCart}
                openModal={openModal}
                closeModal={closeModal}
            />
            {modal}
        </>
    );
}

export default App;