import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Favorites from '../../pages/Favorites/Favorites';
import Cart from '../../pages/Cart/Cart';
import Home from '../../pages/Home/Home';
import Error404 from '../../pages/Error404/Error404';

const AppRoutes = ({ products, isLoading, addToCart, addToFavorite, deleteFromCart, openModal, closeModal }) => {
    return (
        <Switch>
            <Route exact path='/favorites' >
                <Favorites
                    addToCart={addToCart}
                    addToFavorite={addToFavorite}
                    openModal={openModal}
                    closeModal={closeModal}
                />
            </Route>
            <Route exact path='/cart' >
                <Cart
                    deleteFromCart={deleteFromCart}
                    openModal={openModal}
                    closeModal={closeModal}
                />
            </Route>
            <Route exact path='/' >
                <Home
                    products={products}
                    isLoading={isLoading}
                    addToCart={addToCart}
                    addToFavorite={addToFavorite}
                    openModal={openModal}
                    closeModal={closeModal}
                />
            </Route>
            <Route path='*'><Error404 /></Route>
        </Switch>
    );
}

AppRoutes.propTypes = {
    products: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}

export default AppRoutes;