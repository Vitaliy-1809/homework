const ADD_PRODUCTS = 'use_redux/products/ADD_PRODUCTS';
const REMOVE_PRODUCTS = 'use_redux/products/REMOVE_PRODUCTS';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    ADD_PRODUCTS,
    REMOVE_PRODUCTS
}