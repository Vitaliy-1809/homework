const getProducts = () => state => state.products.data;
const isProductsLoading = () => state => state.products.isLoading;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getProducts,
    isProductsLoading
}