import reducer from "./reducer";

export { default as productsSelectors } from './selectors';
export { default as productsOperations } from './operations';

export default reducer;