import actions from './actions';
const { addFavorites } = actions;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    addFavorites
}