import { combineReducers } from "redux";
import modal from './modal';
import products from './products';
import favorites from './favorites';
import cart from './cart';

const reducer = combineReducers({
    modal,
    products,
    favorites,
    cart
});

export default reducer;