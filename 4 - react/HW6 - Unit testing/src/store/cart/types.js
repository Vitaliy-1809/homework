const ADD_TO_CART = 'use_redux/cart/ADD_TO_CART';
const CHECKOUT = 'use_redux/cart/CHECKOUT';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    ADD_TO_CART,
    CHECKOUT
}