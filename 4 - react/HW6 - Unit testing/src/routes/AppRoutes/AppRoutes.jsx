import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Favorites from '../../pages/Favorites/Favorites';
import Cart from '../../pages/Cart/Cart';
import Home from '../../pages/Home/Home';
import Error404 from '../../pages/Error404/Error404';

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path='/favorites' >
                <Favorites />
            </Route>
            <Route exact path='/cart' >
                <Cart />
            </Route>
            <Route exact path='/' >
                <Home />
            </Route>
            <Route path='*'>
                <Error404 />
            </Route>
        </Switch>
    );
}

export default AppRoutes;