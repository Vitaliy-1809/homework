import React, { useEffect } from 'react';
import ProductList from '../../components/ProductList/ProductList';
import { useDispatch, useSelector } from 'react-redux';
import { favoritesOperations, favoritesSelectors } from '../../store/favorites';
import { cartOperations } from '../../store/cart';

const Favorites = () => {
    const favorites = useSelector(favoritesSelectors.getFavorites());
    const dispatch = useDispatch();

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites'));

        if (favorites) {
            dispatch(favoritesOperations.addFavorites(favorites));
        }

    }, [dispatch]);

    useEffect(() => {
        const cartStorage = JSON.parse(localStorage.getItem('cart'));

        if (cartStorage) {
            dispatch(cartOperations.addToCart(cartStorage));
        }
    });

    return (
        <div className='products container'>
            <h1 className='header-title'>Favorites</h1>
            {favorites.length === 0 &&
                <p>Nothing has been added to favorites...</p>
            }
            <ProductList />
        </div>
    );
}

export default Favorites;