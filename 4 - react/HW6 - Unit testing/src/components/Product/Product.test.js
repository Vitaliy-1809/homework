import Product from './Product';
import { Provider } from 'react-redux';
import store from '../../store/store';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter } from 'react-router';

const productCard = { id: 1, name: '', price: '100', img: '', code: 111, color: '' };
const isFavorite = true;
const inCart = true;

describe('Test Product.jsx', () => {
    test('Smoke test Product', () => {
        render(
            <Provider store={store}>
                <MemoryRouter>
                    <Product
                        productCard={productCard}
                        isFavorite={isFavorite}
                        inCart={inCart}
                    />
                </MemoryRouter>
            </Provider>
        );
    });
});

describe('Test Buttons in Product.jsx', () => {
    test('click', () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <MemoryRouter>
                    <Product
                        productCard={productCard}
                        isFavorite={isFavorite}
                        inCart={inCart}
                    />
                </MemoryRouter>
            </Provider>
        );

        const buttonStarProductHome = getByTestId('button-star-product-home');
        userEvent.click(buttonStarProductHome);

        const buttonOpenModalProductCart = getByTestId('button-open-modal-product-cart');
        userEvent.click(buttonOpenModalProductCart);
    });
});