import React from 'react'
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import store from '../../store/store';
import CartForm from './CartForm';

jest.mock('react-router-dom', () => ({ Link: 'a' }));

describe('Test CartForm.jsx', () => {
    test('Rendering and submitting CartForm', async () => {
        const handleSubmit = jest.fn();
        render(
            <Provider store={store}>
                <CartForm onSubmit={handleSubmit} />
            </Provider>
        );

        userEvent.type(screen.getByLabelText(/first name/i), 'John')
        userEvent.type(screen.getByLabelText(/second name/i), 'Dee');
        userEvent.type(screen.getByLabelText(/age/i), '12');
        userEvent.type(screen.getByLabelText(/phone/i), '(555)555-55-55');
        userEvent.type(screen.getByLabelText(/city/i), 'Kyiv');
        userEvent.type(screen.getByLabelText(/address/i), 'Podil');

        userEvent.click(screen.getByRole('button', { name: /checkout/i }));

        await waitFor(() =>
            expect(handleSubmit).toHaveBeenCalledWith({
                firstName: 'John',
                secondName: 'Dee',
                age: '12',
                phone: '(555)555-55-55',
                city: 'Kyiv',
                address: 'Podil'
            }),
        )
    });
});