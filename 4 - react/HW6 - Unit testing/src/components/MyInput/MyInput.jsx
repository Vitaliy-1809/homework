import React from 'react';
import './MyInput.scss';

const MyInput = ({ field, form, ...rest }) => {
    const { name } = field;

    return (
        <div>
            <input {...field} {...rest} className='form__input' />
            {form.touched[name] && form.errors[name] && <div className="field-error">{form.errors[name]}</div>}
        </div>
    );
}

export default MyInput;