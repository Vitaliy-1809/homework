import Modal1 from './Modal1';
import Modal from './Modal';
import { Provider } from 'react-redux';
import store from '../../store/store';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const productCard = {};
const actions = <div></div>;
const closeModal = jest.fn();

describe('Test Modal1.js', () => {
    test('Smoke test Modal1', () => {
        render(
            <Provider store={store}>
                <Modal1 productCard={productCard} />
            </Provider>
        );
    });
});

describe('Test Buttons in Modal1.js', () => {
    test('click', () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <Modal1 productCard={productCard}>
                    <Modal
                        actions={actions}
                        closeModal={closeModal}
                    />
                </Modal1>
            </Provider>
        );

        expect(getByTestId('modal')).toBeTruthy();

        const okButton = getByTestId('ok-button');
        userEvent.click(okButton);

        const cancelButton = getByTestId('cancel-button');
        userEvent.click(cancelButton);
    });
});