import Modal from './Modal';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

jest.mock('../Button/Button', () => () => <button data-testid='close-button'>X</button>);
const actions = <div></div>;
const closeModal = jest.fn();

describe('Render Modal and test Button in Modal.jsx', () => {
    test('Smoke test Modal', () => {
        render(
            <Modal
                actions={actions}
                closeModal={closeModal}
            />
        );
    });

    test('close-button click', () => {
        const { getByTestId } = render(
            <Modal
                actions={actions}
                closeModal={closeModal}
            />
        );

        expect(getByTestId('modal')).toBeTruthy();
        const button = getByTestId('close-button');
        userEvent.click(button);
    });
});