import Modal2 from './Modal2';
import Modal from './Modal';
import { Provider } from 'react-redux';
import store from '../../store/store';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const productCard = {};
const actions = <div></div>;
const closeModal = jest.fn();

describe('Test Modal2.js', () => {
    test('Smoke test Modal2', () => {
        render(
            <Provider store={store}>
                <Modal2 productCard={productCard} />
            </Provider>
        );
    });
});

describe('Test Buttons in Modal2.js', () => {
    test('okButton exists and testing click on cancelButton', () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <Modal2 productCard={productCard}>
                    <Modal
                        actions={actions}
                        closeModal={closeModal}
                    />
                </Modal2>
            </Provider>
        );

        expect(getByTestId('modal')).toBeTruthy();    

        const okButton = getByTestId('ok-button');
        expect(okButton).toBeTruthy();

        const cancelButton = getByTestId('cancel-button');
        expect(cancelButton).toBeTruthy();
        userEvent.click(cancelButton);
    });
});