import React from 'react';
import './Footer.scss';

const Footer = () => {
    return (
        <footer className='footer'>
            <p className='footer__text'>Copyright &copy; 2021 DAN.IT education</p>
        </footer>
    );
}

export default Footer;