import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { cartSelectors } from '../../store/cart';
import './NavBar.scss';

const NavBar = () => {
    const cart = useSelector(cartSelectors.getCart());

    return (
        <ul className='navbar'>
            <li>
                <NavLink exact to='/' className='navbar__link' activeClassName='navbar__link--active'>Home</NavLink>
            </li>
            <li>
                <NavLink exact to='/favorites' className='navbar__link' activeClassName='navbar__link--active'>Favorites</NavLink>
            </li>
            <li>
                <NavLink exact to='/cart' className='navbar__link' activeClassName='navbar__link--active'>Cart({cart.length})</NavLink>
            </li>
        </ul>
    );
}

export default NavBar;