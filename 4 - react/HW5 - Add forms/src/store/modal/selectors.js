const getModal = () => state => state.modal;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getModal
}