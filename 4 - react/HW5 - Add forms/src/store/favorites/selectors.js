const getFavorites = () => state => state.favorites.data;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getFavorites
}