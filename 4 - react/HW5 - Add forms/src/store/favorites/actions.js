import types from './types';

const addFavorites = (products) => ({
    type: types.ADD_FAVORITES,
    payload: products
})

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    addFavorites
}