import types from './types';

const addProducts = (products) => ({
    type: types.ADD_PRODUCTS,
    payload: products
});

const removeProducts = (products) => ({
    type: types.REMOVE_PRODUCTS,
    payload: products
});

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    addProducts,
    removeProducts
}