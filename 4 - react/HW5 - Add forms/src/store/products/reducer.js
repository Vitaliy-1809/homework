import types from "./types";

const initialState = {
    data: [],
    isLoading: true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_PRODUCTS: {
            return { ...state, data: action.payload, isLoading: false }
        }
        case types.REMOVE_PRODUCTS: {
            return { ...state, data: action.payload, isLoading: false }
        }
        default:
            return state
    }
}

export default reducer;