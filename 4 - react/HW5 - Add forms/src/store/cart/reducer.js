import types from "./types";

const initialState = {
    data: [],
    isLoading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_TO_CART: {
            return { ...state, data: action.payload }
        }
        case types.CHECKOUT: {
            return { ...state, data: action.payload }
        }
        default:
            return state
    }
}

export default reducer;