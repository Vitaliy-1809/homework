import types from './types';

const addToCart = (products) => ({
    type: types.ADD_TO_CART,
    payload: products
});

const checkout = () => ({
    type: types.CHECKOUT,
    payload: localStorage.removeItem('cart') || []
});

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    addToCart,
    checkout
}