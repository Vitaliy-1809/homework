import React from 'react';
import NumberFormat from 'react-number-format';

const MyPhoneInput = ({ field, form, ...rest }) => {
    const { name } = field;

    return (
        <div>
            <NumberFormat format="(###)###-##-##" mask="#" allowEmptyFormatting {...field} {...rest} className='form__input' />
            {form.touched[name] && form.errors[name] && <div className="field-error">{form.errors[name]}</div>}
        </div>
    );
}

export default MyPhoneInput;