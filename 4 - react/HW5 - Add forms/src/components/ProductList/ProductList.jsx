import React from 'react';
import './ProductList.scss';
import Product from '../Product/Product';
import { useSelector } from 'react-redux';
import { productsSelectors } from '../../store/products';
import { useLocation } from 'react-router';
import { favoritesSelectors } from '../../store/favorites';
import { cartSelectors } from '../../store/cart';

const ProductList = () => {
    const location = useLocation();

    const products = useSelector(productsSelectors.getProducts());
    const favorites = useSelector(favoritesSelectors.getFavorites());
    const cart = useSelector(cartSelectors.getCart());

    const productCards = products.map(product => (
        <Product
            key={product.id}
            productCard={product}
            isFavorite={product.isFavorite}
            inCart={product.inCart}
        />
    ));

    const favoriteProducts = favorites.map(product => (
        <Product
            key={product.id}
            productCard={product}
            isFavorite={product.isFavorite}
            inCart={product.inCart}
        />
    ));

    const cartProducts = cart.map(product => (
        <Product
            key={product.id}
            productCard={product}
            isFavorite={product.isFavorite}
            inCart={product.inCart}
        />
    ));

    return (
        <>
            <div className='products-list'>
                {location.pathname === '/' && productCards}
                {location.pathname === '/favorites' && favoriteProducts}
            </div>
            {location.pathname === '/cart' && <div className='products-list cart-products'>
                <h2 className='cart-products__header'>Ordered products</h2>
                {cartProducts}
            </div>}
        </>
    );
}

export default ProductList;