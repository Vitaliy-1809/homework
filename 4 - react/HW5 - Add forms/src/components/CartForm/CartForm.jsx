import { Field, Form, Formik } from 'formik';
import React from 'react';
import './CartForm.scss';
import CartFormSchema from './CartFormSchema';
import MyInput from '../../components/MyInput/MyInput';
import { Link } from 'react-router-dom';
import Button from '../Button/Button';
import MyPhoneInput from '../MyPhoneInput/MyPhoneInput';
import { useDispatch, useSelector } from 'react-redux';
import { cartOperations } from '../../store/cart';
import { productsOperations, productsSelectors } from '../../store/products';

const CartForm = () => {
    const products = useSelector(productsSelectors.getProducts());
    const dispatch = useDispatch();

    return (
        <Formik
            initialValues={{
                name: '',
                secondName: '',
                age: '',
                phone: '',
                city: '',
                address: ''
            }}
            validationSchema={CartFormSchema}
            onSubmit={values => {
                setTimeout(() => {
                    const cartStorage = JSON.parse(localStorage.getItem('cart'));
                    console.log('purchased products:', cartStorage);
                    console.log('form data:', JSON.stringify(values, null, 2));

                    const updatedProducts = products.map(product => {
                        return { ...product, inCart: false }
                    })

                    dispatch(productsOperations.removeProducts(updatedProducts));
                    dispatch(cartOperations.checkout());
                }, 500);
            }}
        >
            {() => {
                return (
                    <div className='form-container'>
                        <Form className='form'>
                            <h2 className='form__header'>Order placement</h2>

                            <div className='wrapper'>
                                <h4>Customer information</h4>
                                <Link className='form__link' to='/'>Back to shopping</Link>
                            </div>

                            <div>
                                <label htmlFor='name' className='input__label'>Name</label>
                                <Field
                                    component={MyInput}
                                    name='name'
                                    type='text'
                                    placeholder='Please Enter your name'
                                />
                            </div>

                            <div>
                                <label htmlFor='secondName' className='input__label'>Second name</label>
                                <Field
                                    component={MyInput}
                                    name='secondName'
                                    type='text'
                                    placeholder='Please Enter your second name'
                                />
                            </div>

                            <div>
                                <label htmlFor='age' className='input__label'>Age</label>
                                <Field
                                    component={MyInput}
                                    name='age'
                                    type='number'
                                    placeholder='Please Enter your age'
                                />
                            </div>

                            <div>
                                <label htmlFor='phone' className='input__label'>Phone</label>
                                <Field
                                    component={MyPhoneInput}
                                    name='phone'
                                    type='tel'
                                />
                            </div>

                            <h4>Delivery information</h4>
                            <div>
                                <label htmlFor='city' className='input__label'>City</label>
                                <Field
                                    component={MyInput}
                                    name='city'
                                    type='text'
                                    placeholder='Please Enter your City'
                                />
                            </div>

                            <div>
                                <label htmlFor='address' className='input__label'>Address:</label>
                                <Field
                                    component={MyInput}
                                    name='address'
                                    type='text'
                                    placeholder='Please Enter your Address'
                                />
                            </div>

                            <p className='required-fields'>Required fields</p>

                            <Button
                                className='btn btn-form-submit'
                                type='submit'
                                backgroundColor='#1e1e20'
                                text='Checkout'
                            />
                        </Form>
                    </div>
                )
            }}
        </Formik>
    );
}

export default CartForm;