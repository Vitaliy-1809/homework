import * as Yup from 'yup';

const IS_REQUIRED = 'This field is required';
const PHONE_REGEX = /(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?/;

const CartFormSchema = Yup.object().shape({
    name: Yup
        .string()
        .min(3, 'Must be longer than 3 characters')
        .max(20, 'Nice try, nobody has a first name that long')
        .required(IS_REQUIRED),
    secondName: Yup
        .string()
        .min(3, 'Must be longer than 3 characters')
        .max(20, 'Nice try, nobody has a second name that long')
        .required(IS_REQUIRED),
    age: Yup
        .number()
        .min(10, 'Minimum 2 digits required')
        .required(IS_REQUIRED),
    phone: Yup
        .string()
        .matches(PHONE_REGEX, 'Phone number is not valid')
        .required(IS_REQUIRED),
    city: Yup
        .string()
        .required(IS_REQUIRED),
    address: Yup
        .string()
        .required(IS_REQUIRED)
});

export default CartFormSchema;