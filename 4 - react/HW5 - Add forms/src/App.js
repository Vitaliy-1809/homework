import React, { useEffect } from 'react';
import './App.scss';
import AppRoutes from './routes/AppRoutes/AppRoutes';
import NavBar from './components/NavBar/NavBar';
import { useDispatch, useSelector } from 'react-redux';
import { modalSelectors } from './store/modal';
import { productsOperations } from './store/products';
import Footer from './components/Footer/Footer';

const App = () => {
    const modal = useSelector(modalSelectors.getModal());
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            dispatch(productsOperations.fetchProducts())
        }, 1000)
    }, [dispatch]);

    return (
        <>
            <NavBar />
            <AppRoutes />
            <Footer />
            {modal.modal}
        </>
    );
}

export default App;