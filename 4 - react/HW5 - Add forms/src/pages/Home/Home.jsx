import React, { useEffect } from 'react';
import './Home.scss';
import Loader from '../../components/Loader/Loader';
import ProductList from '../../components/ProductList/ProductList';
import { useDispatch, useSelector } from 'react-redux';
import { productsSelectors } from '../../store/products';
import { cartOperations } from '../../store/cart';

const Home = () => {
    const isLoading = useSelector(productsSelectors.isProductsLoading());
    const dispatch = useDispatch();

    useEffect(() => {
        const cartStorage = JSON.parse(localStorage.getItem('cart'));

        if (cartStorage) {
            dispatch(cartOperations.addToCart(cartStorage));
        }
    });

    return (
        <div className='products container'>
            <h1 className='header-title'>Musical instruments</h1>
            {isLoading && <Loader />}
            {!isLoading && <ProductList />}
        </div>
    );
}

export default Home;