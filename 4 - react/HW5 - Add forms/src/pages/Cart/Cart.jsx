import React, { useEffect } from 'react';
import './Cart.scss';
import ProductList from '../../components/ProductList/ProductList';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { cartOperations, cartSelectors } from '../../store/cart';
import CartForm from '../../components/CartForm/CartForm';

const Cart = () => {
    const cart = useSelector(cartSelectors.getCart());
    const cartStorage = JSON.parse(localStorage.getItem('cart'));
    const dispatch = useDispatch();

    useEffect(() => {
        const cartStorage = JSON.parse(localStorage.getItem('cart'));

        if (cartStorage) {
            dispatch(cartOperations.addToCart(cartStorage));
        }
    }, [dispatch]);

    return (
        <div className='products container'>
            {cart.length === 0 &&
                <>
                    <h1 className='header-title'>Cart</h1>
                    <p>Your cart is empty...</p>
                </>
            }
            <div className='components-wrapper'>
                {cartStorage && <CartForm />}
                <div>
                    {cartStorage && <ProductList />}
                </div>
            </div>
        </div>
    );
}

export default Cart;