import React from 'react';
import './Error404.scss';

const Error404 = () => {
        return (
            <div className='error-404'>
                <img className='error-404__img' src="./img/page_not_found.jpg" alt='error404' />
            </div>
        );
}

export default Error404;