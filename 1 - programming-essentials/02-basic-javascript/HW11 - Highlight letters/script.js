let buttons = document.getElementsByClassName('btn');

document.addEventListener('keydown', function(event) {
    for (let elem of buttons) {
        if (event.key === elem.textContent) {
            elem.classList.add('btn-bgc-blue');
        } else {
            elem.classList.remove('btn-bgc-blue');
        }
    }
});