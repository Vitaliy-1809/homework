let startCount = 1;
let endCount = +prompt('Введите любое число, кратное 5', '5');

if (endCount < 5) {
    console.log('Sorry, no numbers');
}

while (Number.isInteger(endCount) === false) {
    endCount = +prompt('Введите целое число');        
}

for(let i = startCount; i <= endCount; i++) {

    if (i % 5 !== 0) {
        continue;
    }
        
    console.log(`Число, кратное "5": ${i}`);
}