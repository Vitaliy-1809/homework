let arr = ['hello', 'world', 2021, 'january', undefined];

let listItems = arr.map((elem, parent = document.body) => `<li>${elem}</li>`);

let ulList = `<ul>${listItems.join(' ')}</ul>`;

document.body.insertAdjacentHTML('afterend', ulList);