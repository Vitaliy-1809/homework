let arr = ['hello', ['hello, friends', 'hello, neighbors'], 'world', 2021, [1, 2, 3, 4, 5], 'january', undefined];

// Рекурсия, метод map (согласно ТЗ)

const createHtmlFromArray = (arr) => {
    return `<ul>${arr.map((el) => Array.isArray(el) ? createHtmlFromArray(el) : `<li>${el}</li>`).join('')}</ul>`;
}

let ulList = createHtmlFromArray(arr);

document.body.insertAdjacentHTML('afterend', ulList);


// Альтернативный вариант 

// let generateList = function generateList(arr) {
//     let ul = document.createElement('ul');
   
//     arr.forEach(function(elem) {
//         let li = document.createElement('li');
//         let childElement;
   
//         if (Array.isArray(elem)) {
//             childElement = generateList(elem);
//         } else {
//             childElement = document.createTextNode(elem);
//         }
   
//         li.appendChild(childElement);
//         ul.appendChild(li);
//     })
   
//     return ul;
// }
   
// document.body.appendChild(generateList(arr));


// countdown timer 3 sec
let div = document.createElement('div');
div.innerHTML = `До очистки содержания страницы осталось: <span id = "timer_inp">4</span> секунд`;
div.style.fontWeight = 'bold';
document.body.prepend(div);

function countdownTimer() {
    let sec = document.getElementById('timer_inp');
    sec.style.color = 'red';
    sec.innerHTML--;
        if (sec.innerHTML < 0){
            div.innerHTML = '';
        } else {
            setTimeout(countdownTimer, 1000);
        }
}
countdownTimer();


// ul clear in 3 sec
let ul = document.querySelector('ul');

function clear(ul) {
    ul.innerHTML = '';
}

setTimeout(clear, 3000, ul);