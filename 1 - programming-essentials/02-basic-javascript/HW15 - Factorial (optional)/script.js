let n = +prompt('Enter a number greater than 0', '1');

while (n === 0) {
    n = +prompt('Enter a number greater than 0', '1');
}

while (isNaN(n) || !n) {
    n = +prompt('You must enter a number greater than 0, try again', '1');
}

function factorial(n) {
    return (n !== 1) ? n * factorial(n - 1) : 1;
}

alert( `factorial from ${n} = ${factorial(n)}` );