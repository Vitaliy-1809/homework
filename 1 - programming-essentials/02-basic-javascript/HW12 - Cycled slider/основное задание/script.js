let images = document.querySelectorAll('img');
let counter = 0;
let timerId;

showImages();

function showImages() {	
	images.forEach(elem => elem.classList.add('hidden')); 
	images[counter].classList.remove('hidden');

	counter++;

	if (counter > (images.length - 1)) {
		counter = 0;
	};

	timerId = setTimeout(showImages, 3000);
}

let stopButton = document.querySelector('.stop-slideshow');

stopButton.addEventListener('click', () => timerId = clearTimeout(timerId));

let playButton = document.querySelector('.refresh-slideshow');

playButton.addEventListener('click', () => {
	if (!timerId) {
		timerId = setTimeout(showImages, 3000);
	}
});