let images = document.querySelectorAll('img');
let counter = 0;
let timerId;
let countdown;

showImages();
countdownTimer();

function showImages() {
	images.forEach(elem => elem.classList.add('hidden')); 
	images[counter].classList.remove('hidden');

	counter++;

	if (counter > (images.length - 1)) {
		counter = 0;
	};

	timerId = setTimeout(showImages, 3000);
}

function countdownTimer () {
	let timer = document.getElementById('timer-input');
	let input = timer.innerHTML;
	input--;

	if (input <= 0) {
		timer.innerHTML = 3;
	} else {
		timer.innerHTML = input;
	}
	countdown = setTimeout(countdownTimer, 1000);
}

function resetTimer() {
	clearTimeout(countdown);
	countdown = setTimeout(countdownTimer, 1000);

	let timer = document.getElementById('timer-input');
	timer.innerHTML = 3;
}

let stopButton = document.querySelector('.stop-slideshow');

stopButton.addEventListener('click', () => {
	timerId = clearTimeout(timerId);
	clearTimeout(countdown);
});

let playButton = document.querySelector('.refresh-slideshow');

playButton.addEventListener('click', () => {
	if (!timerId) {
		timerId = setTimeout(showImages, 3000);
		
		resetTimer();
	}
});