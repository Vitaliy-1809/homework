let firstName = prompt('Enter your name', '');
let lastName = prompt('Enter your last name', '');
let birthday = prompt('Enter your birthday', 'dd.mm.yyyy');

function createNewUser(firstName, lastName, birthday) {

    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let now = new Date();
            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            let inputDate = +this.birthday.slice(0, 2);
            let inputMonth = +this.birthday.slice(3, 5);
            let inputYear = +this.birthday.slice(6, 10);

            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthDateNow = new Date(today.getFullYear(), birthDate.getMonth(), birthDate.getDate());

            let age = today.getFullYear() - birthDate.getFullYear();
            if (today < birthDateNow) {
                age = age - 1;
            }

            return age;
        }, 
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        }
    };

    return newUser;
}

let user = createNewUser(firstName, lastName, birthday);
console.log(user);

// console.log(user.getLogin());

console.log(user.getAge());
console.log(user.getPassword());