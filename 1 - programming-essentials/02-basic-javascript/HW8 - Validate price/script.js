// const elements = [
//     {
//         tag: 'div'
//     },
//     {
//         tag: 'span',
//         textContent: 'Price, $ '
//     },
//     {
//         tag: 'input',
//         classes: 'price-input',
//         type: 'number'
//     }
// ];

// let createHTMLElement = ({tag, classes, textContent, type, parent = document.body}) => {
//     let e = document.createElement(tag);
//     e.classList.add(classes);
//     e.textContent = textContent;
//     e.type = type;
//     parent.prepend(e);
//     return e;
// };

// const domElems = elements.map(elem => createHTMLElement(elem));
// console.log(domElems);

let input = document.querySelector('.price-input');
input.addEventListener('focus', () => input.classList.add('input-outline-green'));

let spanContainer = document.createElement('div');
spanContainer.classList.add('current-price-container');

let currentPrice = document.createElement('span');

let btn = document.createElement('button');
btn.classList.add('clear-button');
btn.textContent = 'X';

btn.addEventListener('click', () => {
    input.value = '';
    spanContainer.remove();
    input.classList.add('input-color-black');
});

let warningText = document.createElement('div');
warningText.classList.add('warning-text');
warningText.textContent = 'Please enter correct price';

input.addEventListener('blur', showSpan);

function showSpan() {
    if (input.value && input.value >= 0) {

        document.body.prepend(spanContainer);
        spanContainer.prepend(currentPrice, btn);
        currentPrice.textContent = `Текущая цена: ${input.value} $ `;
        input.classList.add('input-color-green');

    } else if (input.value < 0) {

        document.body.append(warningText);
        input.classList.add('input-border-red');
        
    }
};