let btn = document.getElementById('theme-button');
let link = document.getElementById('theme-link');

btn.addEventListener('click', () => changeTheme());

function changeTheme() {
    let lightTheme = './css/light.css';
    let darkTheme = './css/dark.css';
    let currTheme = link.getAttribute('href');

    if (currTheme === lightTheme) {
        currTheme = darkTheme;
        localStorage.setItem('theme', 'dark-mode');
    } else {    
   	    currTheme = lightTheme;
        localStorage.setItem('theme', 'light-mode');
    }

    link.setAttribute('href', currTheme);
}

window.onload = () => {
    let darkTheme = './css/dark.css';
    let currTheme = localStorage.getItem('theme');

    if (currTheme === 'dark-mode') {
        link.setAttribute('href', darkTheme);
    }
}