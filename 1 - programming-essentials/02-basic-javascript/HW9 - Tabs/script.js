let tabs = document.querySelectorAll('.js-tab-trigger');
let tabContents = document.querySelectorAll('.js-tab-content');
    
tabs.forEach((trigger) => {

   trigger.addEventListener('click', function(event) {
       let id = event.target.getAttribute('data-tab');
       let content = document.querySelector(`.js-tab-content[data-tab="${id}"]`); 
       let activeTab = document.querySelector('.js-tab-trigger.active');
       let activeContent = document.querySelector('.js-tab-content.active');
       
       activeTab.classList.remove('active');
       trigger.classList.add('active');
       
       activeContent.classList.remove('active');
       content.classList.add('active');
   });
   
});