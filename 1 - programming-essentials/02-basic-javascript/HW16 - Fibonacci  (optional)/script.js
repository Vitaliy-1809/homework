let F0 = 1;
let F1 = 1;
let n = +prompt('Enter a number', '');

function fib(n, F0, F1) {

    if (n < 0) {
        return fib(n + 2, F0, F1) - fib(n + 1, F0, F1);
    } else if (n <= 1) {
        return n;
    } else {
        return fib(n - 2, F0, F1) + fib(n - 1, F0, F1);
    }

    // Альтернативный вариант:
    // return n < 2 ? n < 0 ? fib(n + 2, F0, F1) - fib(n + 1, F0, F1) : n : fib(n - 2, F0, F1) + fib(n - 1, F0, F1);

}

alert( `Fibonacci number from ${n} = ${fib(n, F0, F1)}` );


// Шкала для проверки:
// n	…	−10	−9	−8	−7	−6	−5	−4	−3	−2	−1	0	1	2	3	4	5	6	7	8	9	10	…
// Fn	…	−55	34	−21	13	−8	 5	−3	 2	−1	 1	0	1	1	2	3	5	8	13	21	34	55	…