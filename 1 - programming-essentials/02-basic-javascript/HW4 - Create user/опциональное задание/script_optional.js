function createNewUser() {

    let newUser = {
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName() {
            Object.defineProperty(this, 'firstName', {
                value: prompt('Enter your new name', '')
            });
        },
        setLastName() {
            Object.defineProperty(this, 'lastName', {
                value: prompt('Enter your new last name', '')
            });
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        value: prompt('Enter your name', ''),
        writable: false,
        configurable: true
    });

    Object.defineProperty(newUser, 'lastName', {
        value: prompt('Enter your last name', ''),
        writable: false,
        configurable: true
    });

    return newUser;
}

let user = createNewUser();
console.log(user);

console.log(user.getLogin());

user.firstName = 'Jim';
user.lastName = 'Morrison';
console.log(user);

user.setFirstName();
user.setLastName();
console.log(user);