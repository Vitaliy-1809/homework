let arr = ['hello', 'world', 23, '23', null, true, {}, undefined, Symbol()];

function filterBy(arr, dataType) {
    return arr.filter(elem => typeof(elem) !== dataType || elem === null);
}

function chooseType() {
    dataType = prompt('Choose data type', 'string');
    return dataType;
}

console.log(arr);
console.log(filterBy(arr, chooseType()));