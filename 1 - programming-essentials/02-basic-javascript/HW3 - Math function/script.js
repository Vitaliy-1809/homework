let num1 = +prompt('Enter first number', '');

while (isNaN(num1) || !num1) {
    num1 = +prompt('You must enter a number, try again');
}

let num2 = +prompt('Enter second number', '');

while (isNaN(num2) || !num2) {
    num2 = +prompt('You must enter a number, try again');
}

let operation = prompt('Enter the operation: +, -, *, /');

while (
    operation !== '+' && 
    operation !== '-' && 
    operation !== '*' &&
    operation !== '/'
) {
    operation = prompt('Enter the correct operation: +, -, *, /');
}

function mathOperation(num1, num2, operation) {

    switch (operation) {
        case '+':
            return `result ${num1} + ${num2} = ${num1 + num2}`;
        
        case '-':
            return `result ${num1} - ${num2} = ${num1 - num2}`;

        case '*':
            return `result ${num1} * ${num2} = ${num1 * num2}`;

        case '/': 
            return `result ${num1} / ${num2} = ${num1 / num2}`;
    }

}

console.log(mathOperation(num1, num2, operation)); 