let form = document.getElementsByClassName('password-form')[0];
let inputs = document.querySelectorAll('input');

form.addEventListener('click', (event) => {
    let eyeIcon = event.target.closest('i');
    if (!eyeIcon) return;

    let parent = event.target.parentElement;
    let input = parent.querySelector('input');

    if (input.getAttribute('type') === 'password') {
        eyeIcon.classList.add('fa-eye-slash');
        input.setAttribute('type', 'text');
    } else {
        eyeIcon.classList.remove('fa-eye-slash');
        input.setAttribute('type', 'password');
    }
});

let confirmButton = document.querySelector('.btn');
let inputEnter = document.getElementById('enter-pass');
let inputCheck = document.getElementById('check-pass');

confirmButton.addEventListener('click', () => {
    if (inputEnter.value && inputCheck.value && inputEnter.value === inputCheck.value) {
        alert('You are welcome');
    } else {
        document.querySelector('.warning-text').classList.remove('hidden');
    }
});