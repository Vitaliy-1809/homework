$(function() {

    $('#menu').on('click','a', function(event) {
        event.preventDefault();
        let id  = $(this).attr('href');
        let top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    }); 
    
    $(window).scroll(function() {
        if ($(this).scrollTop() > $(this).height()) {
            if ($('.up-button').is(':hidden')) {
                $('.up-button').css({opacity : 1}).fadeIn('slow');
            }
        } else { $('.up-button').stop(true, false).fadeOut('fast'); }
    });
    $('.up-button').click(function() {
        $('html, body').stop().animate({scrollTop : 0}, 1500);
    });

    $('#hide-section').click(function() {
        $('#most-popular-posts').slideToggle('slow');
    });

});