class locationCheck {
    constructor() {
        this.elements = {
            button: document.createElement('button'),
            locationInfo: document.createElement('p')
        }
        this.data = this.getIP();
    }

    async getIP() {
        const IPRequest = await fetch('https://api.ipify.org/?format=json');
        const userIP = await IPRequest.json();

        const locationRequest = await fetch(`http://ip-api.com/json/${userIP.ip}?fields=continent,country,regionName,city,lon,lat`);
        const userLocation = await locationRequest.json();

        return userLocation;
    }

    async render() {
        const { button, locationInfo } = this.elements;
        const { continent, country, regionName, city, lon, lat } = await this.data;

        button.textContent = 'Вычислить по IP';

        button.addEventListener('click', () => {
            locationInfo.textContent = `
                Continent: ${continent}, 
                country: ${country}, 
                region: ${regionName}, 
                city: ${city}, 
                coordinates: ${lon}, ${lat}`;
            button.after(locationInfo);
        })

        document.body.prepend(button);
    }
}

const calculateByIP = new locationCheck();
calculateByIP.render();