const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70 
    }, 
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    }, 
    { 
        name: "Тысячекратная мысль",
        price: 70
    }, 
    { 
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    }, 
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

debugger;

function validateObj(author, name, price) {
    if (author && name && price) {
        return `<li>author: ${author}</li> <li>name: ${name}</li> <li>price: ${price}</li></br>`;
    } else if (!author) {
        throw new Error('author is undefined');
    } else if (!name) {
        throw new Error('name is undefined');
    } else if (!price) {
        throw new Error('price is undefined');
    }
}

function showUlList(arr, HTMLElement) {
    let listItems = arr.map( ( {author, name, price} ) => {
        try {
            const check = validateObj(author, name, price);
            return check;
        } catch(e) {
            console.error(`Missing property in object: ${e.message}`);
        }
    });

    let ulList = `<ul><b>Books with all properties: </b>${listItems.join(' ')}</ul>`;

    let container = document.getElementById(HTMLElement);
    container.insertAdjacentHTML('beforeend', ulList);
}

showUlList(books, 'root');


// ---------------------------

// Базовый вариант реализации:

// function showUlList(arr, HTMLElement) {
//     let listItems = arr.map( ( {author, name, price} ) => {
//         try {
//             if (author && name && price) {
//                 return `<li>author: ${author}</li> <li>name: ${name}</li> <li>price: ${price}</li></br>`;
//             } else if (!author) {
//                 throw new Error('author is undefined');
//             } else if (!name) {
//                 throw new Error('name is undefined');
//             } else if (!price) {
//                 throw new Error('price is undefined');
//             } 
//         } catch(e) {
//             console.error(`Missing property in object: ${e.message}`);
//         }
//     });

//     let ulList = `<ul><b>Books with all properties: </b>${listItems.join(' ')}</ul>`;

//     let container = document.getElementById(HTMLElement);
//     container.insertAdjacentHTML('beforeend', ulList);
// }

// showUlList(books, 'root');