class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return `${this._name} is a new Employee`;
    }

    set name(value) {
        if (!value.length) {
            alert('You should enter a name');
            return;
        }
        this._name = value;
    }

    get age() {
        return `${this._age} years old`;
    }

    set age(value) {
        this._age = value; 
    }

    get salary() {
        return `salary: ${this._salary} (for a trial period)`;
    }

    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return `salary: ${this._salary * 3} (after trial period)`;
    }

    set salary(value) {
        super.salary(value);
    }

}

let programmer = new Programmer('Mike', 23, 800, ['JS', 'C#', 'Python']);
let programmer2 = new Programmer('Casey', 25, 1000, ['C++', 'JS', 'Java']);
let programmer3 = new Programmer('Niki', 21, 600, ['Java', 'PHP', 'R']);

console.log(programmer);
console.log(programmer.salary);

console.log(programmer2);
console.log(programmer2.salary);

console.log(programmer3);
console.log(programmer3.salary);