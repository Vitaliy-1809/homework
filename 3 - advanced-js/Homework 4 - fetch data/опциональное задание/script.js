function elements() {
    return {
        parent: document.querySelector('.filmsContainer'),
        container: document.createElement('div'),
        list: document.createElement('ul')
    }
}

function fetchData() {
    const url = 'https://ajax.test-danit.com/api/swapi/films';

    return fetch(url)
        .then(response => response.json())
}

function showMovies() {
    fetchData()
    .then(films => {
        films.forEach(film => {
            const DOMElements = elements();
            const { parent, container, list } = DOMElements;

            const { episodeId, name, openingCrawl, characters } = film;

            container.insertAdjacentHTML('afterbegin', `
                <h3>Episode #${episodeId}</h3>
                <p><b>Title:</b> ${name}</p>
                <p><b>Short description:</b> ${openingCrawl}</p>
                <h4>Characters:</h4>
                <img src='preloader.gif' alt='a preloader image' id='preloader'>
            `);

            const charactersRequest = characters.map(character => fetch(character));

            showCharacters(charactersRequest, list);
            
            container.append(list);
            parent.append(container);
        });
    });
}

function showCharacters(charactersRequest, list) {
    Promise.all(charactersRequest)
        .then(responses => Promise.all(responses.map(r => r.json())))
        .then(characters => {
            characters.forEach(character => {
                const preloader = document.getElementById('preloader');
                preloader ? preloader.remove() : null;

                const { name } = character;
                list.insertAdjacentHTML('beforeend', `<li>${name}</li>`);
            });
        });
}

showMovies();


// Базовая реализация

// const url = 'https://ajax.test-danit.com/api/swapi/films';

// fetch(url)
//     .then(response => response.json())
//     .then(films => {
//         films.forEach(film => {
//             const filmsContainer = document.querySelector('.filmsContainer');
//             const filmsInfo = document.createElement('div');
//             const charactersList = document.createElement('ul');

//             const { episodeId, name, openingCrawl, characters } = film;

//             filmsInfo.insertAdjacentHTML('afterbegin', `
//                 <h3>Episode #${episodeId}</h3>
//                 <p><b>Title:</b> ${name}</p>
//                 <p><b>Short description:</b> ${openingCrawl}</p>
//                 <h4>Characters:</h4>
//             `);

//             filmsInfo.insertAdjacentHTML('beforeend', `<img src='preloader.gif' alt='a preloader image' id='preloader'>`);

//             const charactersRequest = characters.map(character => fetch(character));

//             Promise.all(charactersRequest)
//                 .then(responses => Promise.all(responses.map(r => r.json())))
//                 .then(characters => {
//                     characters.forEach(character => {
//                         const preloader = document.getElementById('preloader');
//                         preloader ? preloader.remove() : null;

//                         const { name } = character;
//                         charactersList.insertAdjacentHTML('beforeend', `<li>${name}</li>`);
//                     });
//                 });
            
//             filmsInfo.append(charactersList);
//             filmsContainer.append(filmsInfo);
//         });
//     });